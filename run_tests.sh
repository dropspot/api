#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

#!/usr/bin/env bash
set -e
set -x

virtualenv ./ENV
./ENV/bin/pip install -r ./requirements.txt

## Copy the local settings temaplate, it will be loaded as a docker volumn
cp ./dropspot/dropspot/local_settings/local_settings.py.template ./dropspot/dropspot/local_settings/local_settings.py

## Add test specific settings
cat >> ./dropspot/dropspot/local_settings/local_settings.py <<EOF

DATABASES = {
        'default': {
            'ENGINE': 'django.contrib.gis.db.backends.postgis', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': 'dropspot',                      # Or path to database file if using sqlite3.
            # The following settings are not used with sqlite3:
            'USER': 'docker',
            'PASSWORD': 'docker',
            'HOST': '172.17.42.1',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
            'PORT': '5432',                      # Set to empty string for default.
            }
        }


# Email settings
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


RAVEN_CONFIG = {
        'dsn': 'http://ee9cea784b854222ab076322c58968ed:054f26fcc8264a40b1a115666f6e71c2@sentry.dropspot-app.com/3',
        }
RAVEN_CONFIG_DSN = RAVEN_CONFIG['dsn']


INSTALLED_APPS = INSTALLED_APPS + (
        'raven.contrib.django.raven_compat',
        )

COMPRESS_CSS_HASHING_METHOD = 'content'

EOF

./ENV/bin/python ./dropspot/manage.py test dropspot.tests

