# README #

### Description ###

Platform that makes it dead-easy to geocode an arbitrary web-content. The platform includes

* Web front-end that makes it easy to assign geo-tags to arbitrary web-pages
* API that provides read/write access to the geo-coded information

### Features at glance ###

* Beautiful UI that allows for
    * easy geotagging of any web content by using an interactive map
    * augmenting geotags with images
    * bundling of geo-tagged content in collections
* Username/password or Facebook authentication
* RESTful API (for CRUD operations as well as full-text search) 
    * [API endpoints reference](https://bitbucket.org/dropspot/api/wiki/API%201.0%20Reference)
* Docker deployment script

### System requirements ###

* Ubuntu >12.04
* Python >2.7.3, Python 3 not supported
* Django 1.5.4
* PostgreSQL >9.1 with PostGIS extension
* Elasticsearch >1.0.0
* Docker >0.9 (if deployment is performed with Docker)

### Set up ###
* [Deployment instructions](https://bitbucket.org/dropspot/api/wiki/Deployment)
* Project settings are defined in `dropspot/dropspot/settings.py` (global settings) and  `dropspot/dropspot/local_settings/local_settings.py` (local settings).
* When using non-Docker deployment, dependencies can be extracted in from `Dockerfile`
* `jenkins_build.sh` script is used for launching tests and creating a Docker-based build


### Contribution guidelines ###

Feel free to create a new Pull request if you want to propose a new feature. 

### Who do I talk to? ###

* If you need development support contact us at admin@drpspt.com