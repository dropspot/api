#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

"""URLs for the ``subscribe`` app."""
from django.conf.urls.defaults import patterns, url

from .views import SubscriptionCreateView, SubscriptionDeleteView, SubscriptionList, SubscriptionDetail
from rest_framework.urlpatterns import format_suffix_patterns


api_urlpatterns = patterns('',
        url(r'^$', SubscriptionList.as_view(), name='subscription-list'),
        url(r'^(?P<pk>\d+)/$', SubscriptionDetail.as_view(), name='subscription-detail'),
        )
urlpatterns = format_suffix_patterns(api_urlpatterns)


urlpatterns += patterns( '',
    url(r'^create/(?P<ctype_pk>\d+)/(?P<object_pk>\d+)/$',
        SubscriptionCreateView.as_view(),
        name='subscriptions_create',),
    url(r'^delete/(?P<ctype_pk>\d+)/(?P<object_pk>\d+)/$',
        SubscriptionDeleteView.as_view(),
        name='subscriptions_delete',),
)
