#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

# Django
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from rest_framework import serializers, relations
from rest_framework.reverse import reverse
from models import Subscription
from dropspot.models import Collection


class SubscriptionSerializer(serializers.ModelSerializer):
    content_type = relations.SlugRelatedField(
            queryset=ContentType.objects.filter(name__in=['collection']),
            slug_field='name')
    content_object_url = serializers.SerializerMethodField('_content_object_url')
    content_object = serializers.SerializerMethodField('_content_object')

    class Meta:
        fields = ('content_type', 'object_url', 'content_object', 'modified', 'created')
        model = Subscription

    def _content_object(self, obj, *args, **kwargs):
        request = self.context.get('request', {})
        if not obj:
            return None
        if not obj.content_object:
            return None
        if type(obj.content_object) is Collection:
            # Import here to prevent circular imports
            from dropspot.serializers import CollectionSerializer
            return CollectionSerializer(obj.content_object, context=self.context).data
        raise Exception('Add a Serialiser for this content type')

    def _name(self, obj, *args, **kwargs):
        if obj and obj.content_object:
            return getattr(obj.content_object, 'name', 'No name')
        return 'No Content'


class SimpleSubscriptionSerializer(SubscriptionSerializer):
    content_type = serializers.SerializerMethodField('content_type_name')

    def content_type_name(self, obj, *args, **kwargs):
        if obj and obj.content_type:
            return getattr(obj.content_type, 'name', '')
        return ''

    class Meta:
        fields = ('content_type', 'content_object', 'modified', 'created')
        model = Subscription


