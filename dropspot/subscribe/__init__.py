#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

# -*- coding: utf-8 -*-
__version__ = '0.1.4'
