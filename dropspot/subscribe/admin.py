#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

"""Admin classes for the ``subscribe`` app."""
from django.contrib import admin

from .models import Subscription


admin.site.register(Subscription)
