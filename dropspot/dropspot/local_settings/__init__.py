#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

try:
    from .local_settings import *
except ImportError as e:
    print "Did you copy local_settings.py.template to local_settings.py?", e
