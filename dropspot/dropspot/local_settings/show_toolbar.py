#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

# Only show toolbar for super user
def show_toolbar_superuser(request):
    if request.user.is_superuser:
        return True
    return False