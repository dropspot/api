#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

# -*- coding: utf-8 -*-
import os
import factory
from django.core.files import File
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from dropspot.models import Spot, Collection, CollectionSpot, ContentLink

TEST_MEDIA_PATH = os.path.join(os.path.dirname(__file__), 'tests', 'test_media')
TEST_PHOTO_PATH = os.path.join(TEST_MEDIA_PATH, 'test_photo.png')


class UserFactory(factory.DjangoModelFactory):
    FACTORY_FOR = User

    username = factory.Sequence(lambda n: 'user_{n}'.format(n=n))
    first_name = 'john'
    last_name = factory.Sequence(lambda n: 'snow{n}'.format(n=n))
    password = 'secret'

    @classmethod
    def _prepare(cls, create, **kwargs):
        password = kwargs.pop('password', None)
        user = super(UserFactory, cls)._prepare(create, **kwargs)
        if password:
            user.set_password(password)
            if create:
                user.save()
        return user


class SpotFactory(factory.DjangoModelFactory):
    FACTORY_FOR = Spot

    name = factory.Sequence(lambda n: 'Some Test Spot {n}'.format(n=n))
    slug = factory.LazyAttribute(lambda a: slugify(a.name))
    created_by = factory.SubFactory(UserFactory)
    photo = factory.LazyAttribute(lambda a: File(open(TEST_PHOTO_PATH)))


class CollectionFactory(factory.DjangoModelFactory):
    FACTORY_FOR = Collection

    name = factory.Sequence(lambda n: 'Sample Collection {n}'.format(n=n))
    slug = factory.LazyAttribute(lambda a: slugify(a.name))
    created_by = factory.SubFactory(UserFactory)
    photo = factory.LazyAttribute(lambda a: File(open(TEST_PHOTO_PATH)))


class CollectionSpotFactory(factory.DjangoModelFactory):
    FACTORY_FOR = CollectionSpot

    spot = factory.SubFactory(SpotFactory)
    collection = factory.SubFactory(CollectionFactory)
    created_by = factory.SubFactory(UserFactory)


class ContentLinkFactory(factory.DjangoModelFactory):
    FACTORY_FOR = ContentLink

    spot = factory.SubFactory(SpotFactory)
    picture_link = 'http://www.bild.de/unterhaltung/tv/angela-merkel/bei-wer-wird-millionaer-wolfgang-bosbach-zieht-den-kanzler-joker-36218060.bild.html'
