#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

from rest_framework.views import exception_handler
from rest_framework import status, exceptions


def custom_exception_handler(exc):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc)

    # Now add the HTTP status code to the response.
    if isinstance(exc, (exceptions.NotAuthenticated,
                        exceptions.AuthenticationFailed)):
        # DRF makes this a 403 if there is no www-authentica header
        # we need that to stay a 401 for accuracy
        response.status_code = status.HTTP_401_UNAUTHORIZED

    if response is not None:
        response.data['status_code'] = response.status_code

    return response
