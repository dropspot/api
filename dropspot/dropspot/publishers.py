#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

# Custom publisher actions
def get_BILD_articleID(link):
    url = link.split(",")[0]
    numbers_temp = [s for s in url.split("-")][-1]
    number = numbers_temp.split('.')[0]
    return number


def publisher_BILD_results(link, spots):
    from dropspot.models import ContentLink, Spot
    results = []
    number = get_BILD_articleID(link)
    if number.isdigit():
        spot_links = ContentLink.objects.filter(client_content_id=number)
        spots = [link.spot for link in spot_links if link.spot in spots]
        results = list(spots)
    return results


# All publishers
PUBLISHERS = {'bild':[
        publisher_BILD_results, get_BILD_articleID
        ]}


# access publishers dynamicly | Get filter results
def customPublisher(publisher, queryset, link):
    if publisher in PUBLISHERS:
        return PUBLISHERS[publisher][0](link, queryset)


# get custom content ID
def getPublisherID(publisher, link):
    return PUBLISHERS[publisher][1](link)
