#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

import logging
from rest_framework.permissions import DjangoModelPermissions, SAFE_METHODS
from django.contrib.auth.models import User
from dropspot.models import Collection, Spot
from subscribe.models import Subscription

from avatar.models import Avatar

logger = logging.getLogger(__name__)


class DjangoObjectPermissions(DjangoModelPermissions):
    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': ['%(app_label)s.view_%(model_name)s'],
        'HEAD': ['%(app_label)s.view_%(model_name)s'],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }

    def has_permission(self, request, view):
        model_cls = getattr(view, 'model', None)
        queryset = getattr(view, 'queryset', None)

        if model_cls is None and queryset is not None:
            model_cls = queryset.model

        # Workaround to ensure DjangoModelPermissions are not applied
        # to the root view when using DefaultRouter.
        if model_cls is None and getattr(view, '_ignore_model_permissions', False):
            return True

        if (request.method in SAFE_METHODS and not getattr(view, 'authenticated_users_only', True)):
            return True

        user = request.user

        assert model_cls, ('Cannot apply DjangoModelPermissions on a view that'
                           ' does not have `.model` or `.queryset` property.')

        # There is the chance the request using a bad METHOD
        try:
            perms = self.get_required_permissions(request.method, model_cls)
        except KeyError:
            return False

        if user.is_authenticated():
            if model_cls in [Spot, Collection, Avatar]:
                username = view.kwargs.get('username', None)
                if username:
                    user = User.objects.get(username=username)
                    return request.user == user or request.user.has_perm('change_user', user)

            if model_cls is Spot:
                spot_slug = view.kwargs.get('spot_slug', None)
                collection_slug = view.kwargs.get('collection_slug', None)
                if collection_slug:
                    collection = (Collection.objects.filter(slug=collection_slug) or [None])[0]
                    return (collection and collection.created_by == user) or request.user.has_perm(
                        'change_collection', collection)

            if model_cls is Collection:
                spot_slug = view.kwargs.get('spot_slug', None)
                if spot_slug:
                    spot = Spot.objects.get(slug=spot_slug)
                    return spot.created_by == user or request.user.has_perm('change_spot', spot)

        if not (user and user.is_authenticated()):
            return False
        return user.has_perms(perms)

    def has_object_permission(self, request, view, obj):
        model_cls = getattr(view, 'model', None)
        queryset = getattr(view, 'queryset', None)

        if model_cls is None and queryset is not None:
            model_cls = queryset.model

        # Workaround to ensure DjangoModelPermissions are not applied
        # to the root view when using DefaultRouter.
        if model_cls is None and getattr(view, '_ignore_model_permissions', False):
            return True

        assert model_cls, ('Cannot apply DjangoModelPermissions on a view that'
                           ' does not have `.model` or `.queryset` property.')

        if model_cls is Collection:
            if request.user == obj.created_by:
                return True

        if model_cls is Spot:
            if request.user == obj.created_by:
                return True

        if model_cls is Subscription:
            return obj.user == request.user

        if model_cls is User:
            if obj == request.user:
                return True

        perms = self.get_required_permissions(request.method, model_cls)

        if request.method in SAFE_METHODS:
            is_private = getattr(obj, 'is_private', False)
            # Safe method and is not private
            return is_private is False or request.user.has_perms(perms, obj)

        if model_cls is Avatar:
            return request.user.id == obj.user_id or request.user.has_perms('change_user', obj.user)

        return request.user.has_perms(perms, obj)
