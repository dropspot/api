#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

import unittest
from rest_framework.test import APIClient
from time import sleep
from dropspot import factories as fa


class ApiContentLinkTest(unittest.TestCase):
    def test_api_create_content_link(self):
        client = APIClient()
        user = fa.UserFactory.create()
        client.force_authenticate(user)
        c_spot = fa.CollectionSpotFactory.create(created_by=user)

        # create a link with api
        response = client.post(
            '/collections/{c_spot.collection.slug}/spots/{c_spot.spot.slug}/links/'.format(c_spot=c_spot),
            {'link': 'http://www.google.com'}, format='json')
        self.failUnlessEqual(response.status_code, 201)
        created_link = response.data
        check_res = client.get(
            '/collections/{c_spot.collection.slug}/spots/{c_spot.spot.slug}/links/{link_id}/.json'.format(c_spot=c_spot,
                link_id=created_link['id']))
        self.failUnlessEqual(check_res.data['link'], created_link['link'])


        response2 = client.post(
            '/collections/{c_spot.collection.slug}/spots/{c_spot.spot.slug}/links/'.format(c_spot=c_spot),
            {'link': 'http://www.google.it'}, format='json')
        self.failUnlessEqual(response2.status_code, 201)
        created_link2 = response2.data
        check_res2 = client.get(
            '/collections/{c_spot.collection.slug}/spots/{c_spot.spot.slug}/links/{link_id}/.json'.format(c_spot=c_spot,
                link_id=created_link2['id']))
        self.failUnlessEqual(check_res2.data['link'], created_link2['link'])

        response3 = client.post(
            '/collections/{c_spot.collection.slug}/spots/{c_spot.spot.slug}/links/'.format(c_spot=c_spot),
            {'link': 'http://www.google.nl'}, format='json')
        self.failUnlessEqual(response3.status_code, 201)
        created_link3 = response3.data
        check_res3 = client.get(
            '/collections/{c_spot.collection.slug}/spots/{c_spot.spot.slug}/links/{link_id}/.json'.format(c_spot=c_spot,
                link_id=created_link3['id']))
        self.failUnlessEqual(check_res3.data['link'], created_link3['link'])

        response4 = client.post(
            '/collections/{c_spot.collection.slug}/spots/{c_spot.spot.slug}/links/'.format(c_spot=c_spot),
            {'link': 'http://www.google.us'}, format='json')
        self.failUnlessEqual(response4.status_code, 201)
        created_link4 = response4.data

        '''
        # check different endpoints for link availability
        created_link = response4.data
        response = client.get(
            '/collections/{c_spot.collection.slug}/spots/{c_spot.spot.slug}/links/{link_id}/.json'.format(c_spot=c_spot,
                link_id=created_link['id']))
        self.failUnlessEqual(response.data['link'], created_link['link'])
        '''
        sleep(5)
        # Check link-preview title generation
        result = client.get(
            '/collections/{c_spot.collection.slug}/spots/{c_spot.spot.slug}/links/.json'.format(
                c_spot=c_spot,link_id=created_link['id']))

        self.failUnlessEqual(len(result.data['results']), 4)

        title = u'Google'
        self.failUnlessEqual(result.data['results'][0]['title'].encode('raw_unicode_escape').decode('utf-8'),
                             title.encode('raw_unicode_escape').decode('utf-8'))

        self.failUnlessEqual(result.data['results'][1]['title'].encode('raw_unicode_escape').decode('utf-8'),
                             title.encode('raw_unicode_escape').decode('utf-8'))

        self.failUnlessEqual(result.data['results'][2]['title'].encode('raw_unicode_escape').decode('utf-8'),
                             title.encode('raw_unicode_escape').decode('utf-8'))

        self.failUnlessEqual(result.data['results'][3]['title'].encode('raw_unicode_escape').decode('utf-8'),
                             title.encode('raw_unicode_escape').decode('utf-8'))
