#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

import unittest
from django.test import Client
from rest_framework.test import APIRequestFactory
from dropspot import factories as fa


class ApiSpotsTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.factory = APIRequestFactory()

    def test_spot_endpoint_availibility(self):
        response = self.client.get('/spots/.json')
        self.failUnlessEqual(response.status_code, 200)

    def test_spot_endpoint_detail(self):
        spot = fa.SpotFactory.create()
        response = self.client.get('/spots/%s/.json' % spot.slug)
        self.assertEqual(response.data['slug'], spot.slug)

    def test_spot_endpoint_detail_modified(self):
        spot = fa.SpotFactory.create()
        response = self.client.get('/spots/%s/.json' % spot.slug)
        old_modified = response.data['modified']
        spot.name = "Sample spot changed"
        spot.save()
        response = self.client.get('/spots/%s/.json' % spot.slug)
        self.assertEqual(response.data['name'], "Sample spot changed")
        self.assertNotEqual(response.data['modified'], old_modified)
