#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

import unittest
from rest_framework.test import APIClient
from dropspot import factories as fa
from django.core.urlresolvers import reverse


class ApiChangeUserDetails(unittest.TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_change_user_password(self):
        user = fa.UserFactory.create()
        self.client.force_authenticate(user)

        # REST API change password
        url = reverse('user_password_change')
        response = self.client.post(url, {'oldpassword': 'secret',
                                          'newpassword': 'some_new'},
                                          format='json')

        self.assertEqual(response.data['status'], 'Password changed')

        # REST API Login validation
        url = reverse('auth-token')
        data = {'username': user.username, 'password': 'some_new'}
        response = self.client.post(url, data, format='json')
        self.failUnlessEqual(response.status_code, 200)
        self.assertEqual(response.data['username'], user.username)

    def test_reset_user_account(self):
        user = fa.UserFactory.create(email='test@drpspt.com')

        url = reverse('user_password_reset')
        data = {'email': user.email}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.data['status'], 'Password reset')

    def test_change_user_data(self):
        # REST API change user details
        user = fa.UserFactory.create(username='jamiroquai', email='jamiroquai@disco.disco')
        url = reverse('profile')

        data = { 'email': 'franz.ferdinant@alternati.ve',
                 'first_name': 'franz',
                 'last_name': 'ferdinant'
                }

        self.client.force_authenticate(user)
        response = self.client.put(url, data)

        self.assertEqual(response.data['first_name'], 'franz')
        self.assertEqual(response.data['last_name'], 'ferdinant')
        self.assertEqual(response.data['username'], 'jamiroquai')
        self.assertEqual(response.data['email'], 'franz.ferdinant@alternati.ve')
