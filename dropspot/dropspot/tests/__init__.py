#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

from django.contrib.sites.models import Site
from django.contrib.auth.models import Group, Permission
import unittest


# Global Module Test Setup
def setUpModule():
    Site.objects.create(domain='test.drpspt.com', name='drpspt.com')
    print "Create Site test.drpspt.com"
    default_group = Group.objects.get(name='Default')
    print "Add permissions to Default Group"
    for perm in Permission.objects.all():
        default_group.permissions.add(perm)
