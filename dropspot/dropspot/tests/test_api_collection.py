#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

import unittest
from django.test import Client
from rest_framework.test import APIRequestFactory
from dropspot import factories as fa


class ApiCollectionTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.factory = APIRequestFactory()

    def test_collection_endpoint_availibility(self):
        user = fa.UserFactory.create()
        response = self.client.get('/collections/.json')
        response2 = self.client.get('/users/%s/collections/.json' % user)
        self.failUnlessEqual(response.status_code, 200)
        self.failUnlessEqual(response2.status_code, 200)
