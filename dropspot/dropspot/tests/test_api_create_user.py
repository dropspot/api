#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

import unittest
from rest_framework.test import APIClient, APIRequestFactory
from dropspot import factories as fa
from django.core.urlresolvers import reverse

class ApiCreateUserTest(unittest.TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_api_create_user(self):
        # create default user
        user = fa.UserFactory.create(username='BILD')
        self.failUnlessEqual(user.username, 'BILD')

        # create matching user via api
        url = reverse('user-list')
        data = {'email': 'bild@bild.bi', 'username': 'bild', 'password': '123456'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.data['username'][0], 'A user is already registered with this username.')

        # create some not matching user via api
        other_data = {'email': 'bild99@bild.bi', 'username': 'bild99', 'password': '123456'}
        response = self.client.post(url, other_data, format='json')
        self.assertEqual(response.data['username'], 'bild99')
