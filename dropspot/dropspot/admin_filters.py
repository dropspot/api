#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q
from datetime import datetime


class CollectionPublishedFilter(admin.SimpleListFilter):
    title = _('published')
    parameter_name = 'published'

    def lookups(self, request, model_admin):
        return (
            ('published', _('published Collections')),
            ('private', _('private Collections')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'published':
            return queryset.filter(published=True)

        if self.value() == 'private':
            return queryset.filter(published=False)


class SpotExpirationFilter(admin.SimpleListFilter):
    title = _('expired')
    parameter_name = 'expired'

    def lookups(self, request, model_admin):
        return (
            ('expired', _('expired Spots')),
            ('valid', _('valid Spots')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'valid':
            return queryset.filter(Q(expiration_date__gte=datetime.now()) | Q(expiration_date=None))

        if self.value() == 'expired':
            return queryset.filter(expiration_date__lte=datetime.now())
