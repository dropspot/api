#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

import sys
import signal
import lxml.html as html

# Do some custome Exception Stuff to avoid indefinitely lxml parsing
# after 10 sec break up


def parse_link_for_title(link):
    title = picture_link = None
    try:
        site = html.parse(link)
        root = site.getroot()
        if root is not None:
            title = root.cssselect('meta[property="og:title"]')[0].get('content') if root.cssselect(
                'meta[property="og:title"]') else None

            picture_link = root.cssselect('meta[property="og:image"]')[0].get('content') if root.cssselect(
                'meta[property="og:title"]') else None
        if not title:
            try:
                title = root.cssselect(('head title'))[0].text.strip().split('  -')[0] if root.cssselect(
                    'head title') else None
            except AttributeError:
                return None, None
    except IOError:
        return None, None

    return title, picture_link
