#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

from datetime import datetime
from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_syncdb
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.contrib.auth.models import Permission

from django.conf import settings
from django_extensions.db.fields import AutoSlugField
from django_extensions.db.models import TimeStampedModel
from sorl.thumbnail import ImageField
from subscribe.models import Subscription

# Import signals so that they are registered
from dropspot.signals import (clear_collection_cache, clear_spot_cache,
                                clear_spot_links_cache, link_preview)

from dropspot.publishers import getPublisherID, PUBLISHERS


class PublishManager(models.Manager):
    def publish(self):
        return self.model.objects.filter(published=True)


class Spot(TimeStampedModel, models.Model):
    name = models.TextField(blank=False)
    slug = AutoSlugField(max_length=75, blank=False, editable=False,
                         populate_from='name', unique=True, overwrite=False,
                         help_text=_("The name in all lowercase, suitable for URL identification"))

    point = models.PointField(blank=False, null=False, default='Point(0 0)')
    address = models.TextField(blank=True)
    link = models.URLField(blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL)
    photo = ImageField(upload_to='spot', blank=True)
    expiration_date = models.DateTimeField(blank=True, null=True)

    objects = models.GeoManager()

    @property
    def latitude(self):
        try:
            return self.point.y
        except AttributeError:
            self.point = 'Point(0 0)'
            return 0

    @latitude.setter
    def latitude(self, val):
        try:
            self.point.y = val or 0
        except AttributeError:
            self.point = 'Point(0 0)'
            self.point.y = val

    @property
    def longitude(self):
        try:
            return self.point.x
        except AttributeError:
            self.point = 'Point(0 0)'
            return 0

    @longitude.setter
    def longitude(self, val):
        try:
            self.point.x = val or 0
        except AttributeError:
            self.point = 'Point(0 0)'
            self.point.x = val

    @property
    def collection_slugs(self):
        try:
            return self._collection_slugs
        except AttributeError:
            if self.pk:
                self._collection_slugs = self.collections.values_list('slug', flat=True)
                return self._collection_slugs
            else:
                return ''

    @collection_slugs.setter
    def collection_slugs(self, val):
        try:
            self._collection_slugs, self._collection_slugs_user = val
        except ValueError:
            raise ValueError("Pass an iterable with a list of collection "
                             "slugs and a user who is making the changes")

    def save(self, *args, **kwargs):
        super(Spot, self).save(*args, **kwargs)
        try:
            collection_slugs = self._collection_slugs
        except AttributeError:
            return

        user = self.created_by

        # remove any collections that you originally
        # added and that are not on the list
        remove_collectionspots = self.collectionspot_set.filter(
            created_by=user).exclude(collection__slug__in=collection_slugs)
        remove_collectionspots.delete()

        # New collections you created that are about to be added to the spot
        new_collections = Collection.objects.filter(created_by=user,
                                                    slug__in=set(collection_slugs))

        for collection in new_collections:
            collection.modified_spots = self.modified
            collection.save()
            CollectionSpot.objects.get_or_create(collection=collection,
                                                 spot=self,
                                                 defaults={'created_by': user})
        # We want the getter to recalculate and cache it again
        del self._collection_slugs

    def delete(self, *args, **kwargs):
        ContentLink.objects.filter(spot=self).delete()

        for spot in CollectionSpot.objects.filter(spot__slug=self.slug):
            spot.collection.modified_spots = datetime.now()
            spot.collection.save()

        super(Spot, self).delete(*args, **kwargs)

    class Meta(object):
        ordering = ['-id']

    def getCollections(self):
        return Collection.objects.filter(slug__in=set(self.collection_slugs))

    def __unicode__(self):
        return self.name


class Collection(TimeStampedModel, models.Model):
    name = models.TextField(blank=True)
    description = models.TextField(blank=True)
    slug = AutoSlugField(max_length=200, blank=False, editable=False,
                         populate_from='name', unique=True, overwrite=False)
    photo = ImageField(upload_to='spot', blank=True)
    spots = models.ManyToManyField(Spot, through='CollectionSpot',
                                   related_name='collections')
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=False,
                                   blank=False)
    subscriptions = generic.GenericRelation(Subscription)
    modified_spots = models.DateTimeField(null=True, blank=True)
    published = models.BooleanField(default=False)

    objects = PublishManager()


class CollectionSpot(TimeStampedModel, models.Model):
    spot = models.ForeignKey(Spot)
    collection = models.ForeignKey(Collection)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL)

    class Meta(object):
        ordering = ['created']
        unique_together = ("spot", "collection")


def add_view_permissions(sender, **kwargs):
    """
    This syncdb hook takes care of adding a view permission too all our
    content types.
    """
    # for each of our content types
    for model in models.get_models():
        # build our permission slug
        content_type = ContentType.objects.get_for_model(model)
        codename = "view_%s" % content_type.model

        # add it
        perm, created = Permission.objects.get_or_create(codename=codename,
                                                         name="Can view %s" % content_type.name,
                                                         defaults={'content_type': content_type})
        if created:
            print "Added view permission for %s" % content_type.name


class ContentLink(TimeStampedModel, models.Model):
    spot = models.ForeignKey(Spot, related_name='contentLinks')
    link = models.URLField(blank=True)
    client_content_id = models.BigIntegerField(blank=True, null=True)
    picture_link = models.URLField(blank=True, null=True, max_length=2000)
    title = models.TextField(blank=True, null=True)

    def save(self, *args, **kwargs):
        if self.spot.created_by.username.lower() in PUBLISHERS:
            number = getPublisherID(self.spot.created_by.username.lower(), self.link)
            if number.isdigit():
                self.client_content_id = number
        super(ContentLink, self).save(*args, **kwargs)
        self.spot.modified = self.modified
        self.spot.save()

    def delete(self, *args, **kwargs):
        self.spot.modified = self.modified
        self.spot.save()
        super(ContentLink, self).delete(*args, **kwargs)

    def __unicode__(self):
        return self.link

# clear cache when save and delete
models.signals.post_save.connect(clear_collection_cache, sender=Collection)
models.signals.pre_delete.connect(clear_collection_cache, sender=Collection)

models.signals.post_save.connect(clear_spot_cache, sender=Spot)
models.signals.pre_delete.connect(clear_spot_cache, sender=Spot)

models.signals.post_save.connect(clear_spot_links_cache, sender=ContentLink)
models.signals.pre_delete.connect(clear_spot_links_cache, sender=ContentLink)

# get link-preview pictures and title
models.signals.post_save.connect(link_preview, sender=ContentLink)

# check for all our view permissions after a syncdb
post_syncdb.connect(add_view_permissions)
