#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.db import models
from django.contrib.auth.models import Group, User
from avatar.models import Avatar
from django.core.cache import cache
#from django.db.models import get_model
from dropspot.parsers import parse_link_for_title


@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    """Create a matching profile whenever a user object is created."""
    if created:
        g, created = Group.objects.get_or_create(name='Default')
        instance.groups.add(g)
        instance.save()


@receiver(pre_delete, sender=Avatar)
@receiver(post_save, sender=Avatar)
def clear_avatar_cache(sender, instance, **kwargs):
    cache.delete("Avatar_%s" % instance.user.username)
    cache.delete("Spot_Creator_%s" % instance.user.id)
    cache.delete("CollectionList_publish")


@receiver(post_save, sender=User)
@receiver(pre_delete, sender=User)
def clear_spot_creator_cache(sender, instance, **kwargs):
    cache.delete("Spot_Creator_%s" % instance.id)
    cache.delete("CollectionList_publish")


# Is called in models.py to avoid circular imports
def clear_collection_cache(sender, instance, **kwargs):
    cache.delete("CollectionDetail_%s" % instance.slug)
    cache.delete("UserCollections_%s" % instance.created_by.username)
    cache.delete("CollectionList_publish")


def clear_spot_cache(sender, instance, **kwargs):
    cache.delete("UserCollections_%s" % instance.created_by.username)
    cache.delete("UserSpots_%s" % instance.created_by.username)
    cache.delete("DetailSpot_%s" % instance.slug)

    for collection in instance.getCollections():
        cache.delete("CollectionDetail_%s" % collection.slug)
        cache.delete("CollectionCount_%s" % collection.slug)
        cache.delete("CollectionSpots_%s" % collection.slug)
        cache.delete("CollectionList_publish")
        cache.delete("Namespace_FuzzyLinkQuery_%s" % collection.slug)


def clear_spot_links_cache(sender, instance, **kwargs):
    cache.delete("SpotLinks_%s" % instance.spot.slug)
    for collection in instance.spot.getCollections():
        cache.delete("CollectionSpots_%s" % collection.slug)


def link_preview(sender, instance, **kwargs):
    ContentLink = models.get_model('dropspot', 'ContentLink')
    models.signals.post_save.disconnect(link_preview, sender=ContentLink)
    instance.title, instance.picture_link = parse_link_for_title(instance.link)
    instance.save()
    models.signals.post_save.connect(link_preview, sender=ContentLink)
