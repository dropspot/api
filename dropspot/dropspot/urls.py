#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

# Django
from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.gis import admin
from django.views.decorators.csrf import csrf_exempt

# 3rd Party
from rest_framework.urlpatterns import format_suffix_patterns
from allauth.socialaccount.providers.facebook.views import login_by_token

# Project
from dropspot import views

admin.autodiscover()


v2_urlpatterns = \
    patterns('',
             url(r'^account/facebook/login/token/$', csrf_exempt(login_by_token), name="facebook-login"),
             url(r'^account/password/change', views.passwordUpdate, name="user_password_change"),
             url(r'^account/password/reset', views.passwordReset, name="user_password_reset"),
             url(r'^account/', include('allauth.urls')),
             url(r'^admin/', include(admin.site.urls)),
    )

v2_api_urlpatterns = \
    patterns('',
             url(r'^$', views.Home.as_view(), name='home'),
             url(r'^spots/$', views.SpotList.as_view(), name='spot-list'),
             url(r'^spots/(?P<spot_slug>[\w-]+)/$', views.SpotDetail.as_view(),
                 name='spot-detail'),
             url(r'^spots/(?P<spot_slug>[\w-]+)/collections/$', views.CollectionList.as_view(),
                 name='collection-list'),

             url(r'^collections/$', views.CollectionList.as_view(),
                 name='collection-list'),
             url(r'^collections/(?P<collection_slug>[\w-]+)/$', views.CollectionDetail.as_view(),
                 name='collection-detail'),
             url(r'^collections/(?P<collection_slug>[\w-]+)/spots/$', views.SpotList.as_view(),
                 name='spot-list'),
             url(r'^collections/(?P<collection_slug>[^/]+)/subscribe/$', views.CollectionSubscription.as_view(),
                 name='collection-subscription'),

             url(r'^collections/(?P<collection_slug>[\w-]+)/spots/(?P<spot_slug>[\w-]+)/$', views.SpotDetail.as_view(),
                 name='collection-detail'),

             url(r'^collections/(?P<collection_slug>[\w-]+)/spots/(?P<spot_slug>[\w-]+)/links/$',
                 views.ContentLinksList.as_view(),
                 name='content-links-list'),

             url(r'^spots/(?P<spot_slug>[\w-]+)/links/$', views.ContentLinksList.as_view(),
                 name='content-links-list'),

             url(r'^collections/(?P<collection_slug>[\w-]+)/spots/(?P<spot_slug>[\w-]+)/links/(?P<link_id>[\d]+)/$',
                 views.ContentLinksDetail.as_view(),
                 name='content-links-detail'),

             url(r'^spots/(?P<spot_slug>[\w-]+)/links/(?P<link_id>[\d]+)/$', views.ContentLinksDetail.as_view(),
                 name='content-links-detail'),

             url(r'^search/$', views.SearchView.as_view(), name='search'),


             url(r'^users/$', views.UserList.as_view(), name='user-list'),
             url(r'^users/(?P<username>[^/]+)/$', views.UserDetail.as_view(),
                 name='user-detail'),
             url(r'^users/(?P<username>[^/]+)/collections/$', views.CollectionList.as_view(),
                 name='collection-list'),
             url(r'^users/(?P<username>[^/]+)/spots/$', views.SpotList.as_view(),
                 name='spot-list'),
             url(r'^users/(?P<username>[^/]+)/subscriptions/$', views.SubscriptionList.as_view(),
                 name='subscription-list'),
             url(r'^users/(?P<username>[^/]+)/avatars/$', views.AvatarList.as_view(),
                 name='avatar-list'),
             url(r'^users/(?P<username>[^/]+)/avatars/(?P<avatar_id>[0-9]+)/$', views.AvatarDetail.as_view(),
                 name='avatar-detail'),
             url(r'^profile/$', views.UserProfile.as_view(), name='profile'),
             url(r'^token/', views.AuthToken.as_view(), name='auth-token'),
    )

v2_urlpatterns += format_suffix_patterns(v2_api_urlpatterns)

urlpatterns = \
    patterns('',
             url(r'^su/(?P<username>.*)/$', 'dropspot.views.su', {'redirect_url': '/'}),
             (r'^avatar/', include('avatar.urls')),

             url(r'^v2/', include(v2_urlpatterns, namespace='v2')),
             url(r'', include(v2_urlpatterns)),
    )

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()

# django-debug-toolbar
if settings.DEBUG:
    import debug_toolbar

    urlpatterns += patterns('', url(r'^__debug__/', include(debug_toolbar.urls)),)
