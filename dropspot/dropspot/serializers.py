#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

import re
import logging
from django.utils.translation import  ugettext_lazy as _
from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework import relations
from rest_framework import fields
from rest_framework.reverse import reverse
from allauth.utils import email_address_exists

from avatar.models import Avatar
from avatar.util import get_primary_avatar

from sorl.thumbnail import get_thumbnail

try:
    import simplejson as json
except ImportError:
    import json

# Project
from dropspot.models import Spot, Collection,  ContentLink

logger = logging.getLogger(__name__)


class CollectionSlugField(serializers.WritableField):
    def to_native(self, obj):
        regex = re.compile(r'\^*\"spot_id\"\s*=\s*(\d+)')
        query = str(obj.query)
        spot_id = regex.findall(query)[0]
        cache_key = "CollectionSlugField_Spot_%s" % str(spot_id)
        slugs = cache.get(cache_key)
        if not slugs:
            slugs = ','.join(obj)
            cache.set(cache_key, slugs)
        return slugs

    def from_native(self, data):
        try:
            collection_slugs = json.loads(data)
            assert isinstance(collection_slugs, list)
        except (ValueError, AssertionError) as e:
            logging.warning(e)
            collection_slugs = set(data.replace(' ', '').split(','))
        try:
            user = self.parent.context['request'].user
        except Exception as e:
            logging.warning(e)
            user = self.parent.object.created_by
        return collection_slugs, user


class AvatarHyperlinkedField(serializers.HyperlinkedIdentityField):
    def get_url(self, obj, view_name, request, format):
        if obj:
            username = self.context['view'].kwargs.get('username', None)
            kwargs = {'username': username, 'avatar_id': obj.id}
            url = reverse('avatar-detail', kwargs=kwargs, request=request, format=format)
            return url


class HyperlinkedAvatarField(serializers.ImageField):
    def to_native(self, obj):
        size = 80
        if obj:
            if 'request' in self.context:
                if not obj.instance.thumbnail_exists(size):
                    obj.instance.create_thumbnail(size)
                return self.context['request'].build_absolute_uri(obj.instance.avatar_url(size))
            return obj.instance.avatar_url(80)
        return None


class AvatarSerializer(serializers.HyperlinkedModelSerializer):
    url = AvatarHyperlinkedField(view_name="avatar-detail")
    #avatar_url = fields.SerializerMethodField('_avatar_url')
    avatar = HyperlinkedAvatarField(allow_empty_file=False, required=True)

    class Meta(object):
        model = Avatar
        fields = ('id', 'url', 'avatar', 'primary', )


class UserSerializer(serializers.HyperlinkedModelSerializer):
    url = relations.HyperlinkedIdentityField(view_name='user-detail',
                                             lookup_field='username')
    name = fields.SerializerMethodField('get_name')
    date_joined = serializers.DateTimeField(read_only=True)
    email = serializers.EmailField(required=True)
    subscriptions = fields.SerializerMethodField('_subscriptions')
    primary_avatar = fields.SerializerMethodField('_avatar_url')
    avatars = serializers.SerializerMethodField('_avatar_list')
    collections = serializers.SerializerMethodField('_collection_list')
    spots = serializers.SerializerMethodField('_spot_list')

    def validate_email(self, attrs, source):
        """
        Verify that the email is unique
        """
        value = attrs[source]
        try:
            request_usermail = self.context['request'].user.email
        except AttributeError:
            request_usermail = None

        if not request_usermail and value:
            if email_address_exists(value):
                raise serializers.ValidationError(_("A user is already registered"
                                                    " with this e-mail address."))
        elif request_usermail and value:
            if value != request_usermail:
                if email_address_exists(value):
                    raise serializers.ValidationError(_("A user is already registered"
                                                        " with this e-mail address."))

        return attrs

    def validate_username(self, attrs, source):
        if 'username' in attrs:
            value = attrs[source]
            try:
                test_user = User.objects.get(username__iexact=value)
                if self.context['request'].user.username != test_user.username:
                    raise serializers.ValidationError(_("A user is already registered"
                                                        " with this username."))
            except ObjectDoesNotExist:
                pass
        return attrs

    def _avatar_url(self, obj):
        CACHE_KEY = "Avatar_%s" % obj.username
        avatar = cache.get(CACHE_KEY)
        if not avatar:
            avatar = get_primary_avatar(obj)
            cache.set(CACHE_KEY, avatar)
        if avatar:
            img_url = avatar.get_absolute_url()
            try:
                return self.context['request'].build_absolute_uri(img_url)
            except:
                return img_url

    def _avatar_list(self, obj, *args, **kwargs):
        if obj:
            kwargs = {'username': obj.username}
            if self.context['format']:
                kwargs['format'] = self.context['format']
            return reverse('avatar-list',
                           kwargs=kwargs,
                           request=self.context.get('request', {}))
        return ''

    def _collection_list(self, obj, *args, **kwargs):
        if obj:
            kwargs = {'username': obj.username}
            if self.context['format']:
                kwargs['format'] = self.context['format']
            return reverse('collection-list',
                           kwargs=kwargs,
                           request=self.context.get('request', {}))
        return ''

    def _spot_list(self, obj, *args, **kwargs):
        if obj:
            kwargs = {'username': obj.username}
            if self.context['format']:
                kwargs['format'] = self.context['format']
            return reverse('spot-list',
                           kwargs=kwargs,
                           request=self.context.get('request', {}))
        return ''

    def _subscriptions(self, obj, *args, **kwargs):
        if obj:
            kwargs = {'username': obj.username}
            if self.context['format']:
                kwargs['format'] = self.context['format']
            return reverse('subscription-list',
                           kwargs=kwargs,
                           request=self.context.get('request', {}))
        return ''

    def get_name(self, obj):
        if obj:
            return obj.get_full_name() or obj.username

    def restore_object(self, attrs, instance=None):
        user = super(UserSerializer, self).restore_object(attrs, instance)
        if 'password' in attrs:
            user.set_password(attrs['password'])
        return user

    def to_native(self, obj):
        ret = super(UserSerializer, self).to_native(obj)
        if 'email' in ret:
            del ret['email']
        if 'password' in ret:
            del ret['password']
        return ret

    class Meta(object):
        model = get_user_model()
        fields = ('url', 'name', 'first_name', 'last_name', 'email', 'username',
                  'primary_avatar', 'avatars', 'collections', 'spots',
                  'subscriptions', 'password', 'date_joined')


class SimpleUserSerializer(UserSerializer):
    def to_native(self, obj):
        CACHE_KEY = "SimpleUserSerializer%s" % obj.username
        user = cache.get(CACHE_KEY)
        if not user:
            user = super(SimpleUserSerializer, self).to_native(obj)
            cache.set(CACHE_KEY, user)
        return user

    class Meta(object):
        model = get_user_model()
        fields = ('url', 'name', 'username', 'primary_avatar',)


# Private ProfileSerializer
class ProfileSerializer(UserSerializer):
    username = serializers.CharField(read_only=True)

    def __init__(self, *args, **kwargs):
        super(ProfileSerializer, self).__init__(*args, **kwargs)
        self.fields.pop('password')

    def to_native(self, obj):
        ret = super(UserSerializer, self).to_native(obj)
        if 'password' in ret:
            del ret['password']
        return ret


class SpotHyperlinkedField(serializers.HyperlinkedIdentityField):
    def get_url(self, obj, view_name, request, format):
        try:
            collection_slug = self.context['view'].kwargs.get('collection_slug', None)
            if collection_slug:
                kwargs = {'collection_slug': collection_slug, 'spot_slug': obj.slug}
                return reverse('collection-detail', kwargs=kwargs, request=request, format=format)
            kwargs = {'spot_slug': obj.slug}
            return reverse('spot-detail', kwargs=kwargs, request=request, format=format)
        except Exception as e:
            logging.error(e)
            return {}


class HyperlinkedPhotoField(serializers.ImageField):
    def to_native(self, obj):
        if obj:
            if 'request' in self.context:
                iphone = settings.MEDIA_URL + unicode(
                    get_thumbnail(obj, '640x1096', crop='center', upscale=True, quality=99))

                small = settings.MEDIA_URL + unicode(
                    get_thumbnail(obj, '500x500', crop='center', upscale=True, quality=99))

                tiny = settings.MEDIA_URL + unicode(get_thumbnail(obj, '80x80', crop='center', upscale=True, quality=99))

                return {
                    'iphone': self.context['request'].build_absolute_uri(iphone),
                    'small': self.context['request'].build_absolute_uri(small),
                    'tiny': self.context['request'].build_absolute_uri(tiny),
                }
        return None


class SimpleSpotSerializer(serializers.ModelSerializer):
    """
    Spot model is the link to discriptions over time and sets
    """
    url = SpotHyperlinkedField(view_name='spot-detail', slug_field='slug', slug_url_kwarg='spot_slug')

    geojson = serializers.SerializerMethodField('_geojson')
    image_url = fields.SerializerMethodField('_image_url')
    created_by = SimpleUserSerializer(many=False, read_only=True)
    collection_slugs = CollectionSlugField()
    photo = HyperlinkedPhotoField(allow_empty_file=True, required=False)
    latitude = fields.DecimalField(required=True)
    longitude = fields.DecimalField(required=True)
    expiration_date = serializers.DateTimeField(required=False)
    contentLinks = serializers.RelatedField(many=True, read_only=False)

    def _geojson(self, obj):
        """ Serialize the point's geojson """
        return obj and obj.point and json.loads(obj.point.geojson)

    class Meta(object):
        depth = 1
        fields = (
            'url', 'name', 'slug', 'photo', 'address', 'latitude', 'longitude',
            'geojson', 'created_by', 'created', 'modified', 'expiration_date',
            'collection_slugs', 'contentLinks')

        model = Spot


class CollectionSerializer(serializers.ModelSerializer):
    url = relations.HyperlinkedIdentityField(view_name='collection-detail',
                                             slug_field='slug',
                                             slug_url_kwarg='collection_slug')
    photo = HyperlinkedPhotoField(allow_empty_file=True, required=False)
    name = serializers.CharField()
    description = serializers.CharField(required=False)
    created_by = serializers.SerializerMethodField('_created_by')
    current_spot = fields.SerializerMethodField('_current_spot')
    subscribe_url = serializers.SerializerMethodField('_subscribe_url')
    spots = serializers.SerializerMethodField('_spot_list')
    spot_count = serializers.SerializerMethodField('_spot_count')
    modified_spots = serializers.DateTimeField(read_only=True)
    published = serializers.BooleanField(required=False)

    def _spot_count(self, obj):
        if obj:
            CACHE_KEY = "CollectionCount_%s" % obj.slug
            count = cache.get(CACHE_KEY)
            if not count:
                count = obj.spots.count()
                cache.set(CACHE_KEY, count)
            return count

    def _spot_list(self, obj):
        if obj:
            kwargs = {'collection_slug': obj.slug}
            if self.context['format']:
                kwargs['format'] = self.context['format']
            return reverse('spot-list',
                           kwargs=kwargs,
                           request=self.context.get('request', {}))
        return ''

    def _subscribe_url(self, obj):
        if obj:
            kwargs = {'collection_slug': obj.slug}
            if self.context['format']:
                kwargs['format'] = self.context['format']
            return reverse('collection-subscription',
                           kwargs=kwargs,
                           request=self.context.get('request', {}))
        return ''

    # Hacky but can't find an other solution to get maximum cache
    def _created_by(self, obj):
        if obj:
            CACHE_KEY = "Spot_Creator_%s" % obj.created_by_id
            user = cache.get(CACHE_KEY)
            if not user:
                user = User.objects.get(username=obj.created_by.username)
                try:
                    url = get_primary_avatar(user).get_absolute_url()
                    user.avatar_url = self.context['request'].build_absolute_uri(url)
                except:
                    user.avatar_url = None
                cache.set(CACHE_KEY, user)
            kwargs = {'username': user.username}
            if self.context['format']:
                kwargs['format'] = self.context['format']
            return {
                'url': reverse('user-detail', kwargs=kwargs, request=self.context.get('request', {})),
                'name': user.get_full_name(),
                'username': user.username,
                'primary_avatar': user.avatar_url
            }

    class Meta(object):
        fields = ('url', 'name', 'description', 'photo', 'slug', 'subscribe_url',
                  'spots', 'spot_count', 'created_by', 'created', 'modified', 'modified_spots', 'published')
        lookup_field = 'slug'
        lookup_url_kwarg = 'collection_slug'
        model = Collection


class SimpleCollectionSerializer(CollectionSerializer):
    class Meta(object):
        fields = ('url', 'name', 'slug', 'photo', 'created_by', 'created', 'modified')
        lookup_field = 'slug'
        lookup_url_kwarg = 'collection_slug'
        model = Collection


class SpotSerializer(serializers.ModelSerializer):
    url = SpotHyperlinkedField(view_name='spot-detail', slug_field='slug', slug_url_kwarg='spot_slug')
    geojson = serializers.SerializerMethodField('_geojson')
    photo = HyperlinkedPhotoField(allow_empty_file=True, required=False)
    created_by = serializers.SerializerMethodField('_created_by')
    geojson = serializers.SerializerMethodField('_geojson')
    collection_slugs = CollectionSlugField(required=False)
    latitude = fields.DecimalField(required=True)
    longitude = fields.DecimalField(required=True)
    collections = serializers.SerializerMethodField('_collection_list')
    expiration_date = serializers.DateTimeField(required=False)
    content_links = serializers.SerializerMethodField('_content_links_list')

    def __init__(self, *args, **kwargs):
        super(SpotSerializer, self).__init__(*args, **kwargs)
        center = self.context['request'].QUERY_PARAMS.get('center')
        if center:
            self.fields['center_distance'] = serializers.SerializerMethodField('_get_distance')

    def _get_distance(self, obj):
        center = self.context['request'].QUERY_PARAMS.get('center')
        try:
            if center:
                center = center.split(',')
                return {
                    'distance_meter': obj.distance.m,
                    'center': {
                        'latitude': float(center[0]),
                        'longitude': float(center[1])
                    }
                }
        except AttributeError:
            return ""

    def _collection_list(self, obj):
        if obj:
            kwargs = {'spot_slug': obj.slug}
            if self.context['format']:
                kwargs['format'] = self.context['format']
            return reverse('collection-list',
                           kwargs=kwargs,
                           request=self.context.get('request', {}))
        return ''

    # Hacky but can't find an other solution to get maximum cache
    def _created_by(self, obj):
        if obj:
            CACHE_KEY = "Spot_Creator_%s" % obj.created_by_id
            user = cache.get(CACHE_KEY)
            if not user:
                user = User.objects.get(username=obj.created_by.username)
                try:
                    url = get_primary_avatar(user).get_absolute_url()
                    user.avatar_url = self.context['request'].build_absolute_uri(url)
                except:
                    user.avatar_url = None
                cache.set(CACHE_KEY, user)
            kwargs = {'username': user.username}
            if self.context['format']:
                kwargs['format'] = self.context['format']
            return {
                'url': reverse('user-detail', kwargs=kwargs, request=self.context.get('request', {})),
                'name': user.get_full_name(),
                'username': user.username,
                'primary_avatar': user.avatar_url
            }


    def _geojson(self, obj):
        """ Serialize the point's geojson """
        return obj and obj.point and json.loads(obj.point.geojson)

    class Meta(object):
        fields = (
            'url', 'name', 'link', 'slug', 'photo', 'address', 'latitude', 'longitude',
            'geojson', 'created_by', 'created', 'modified', 'expiration_date',
            'collection_slugs', 'collections', 'content_links')
        lookup_field = 'spot_slug'
        model = Spot

    def _content_links_list(self, obj):
        if obj:
            kwargs = {'spot_slug': obj.slug}

            collection_slug = self.context['view'].kwargs.get('collection_slug', None)
            if collection_slug:
                kwargs.update({'collection_slug': self.context['view'].kwargs['collection_slug']})

            CACHE_KEY = "SpotLinks_%s" % obj.slug
            links = cache.get(CACHE_KEY)
            if not links:
                links = dict()
                links['result'] = [link.link for link in ContentLink.objects.filter(spot__slug=obj.slug)]
                cache.set(CACHE_KEY, links)
            if self.context['format']:
                kwargs['format'] = self.context['format']
            return {'url': reverse('content-links-list',
                                   kwargs=kwargs,
                                   request=self.context.get('request', {})),
                    'links': links['result']
            }
        return ''


class DistanceSerializer(serializers.Serializer):
    km = serializers.FloatField()
    m = serializers.FloatField()
    mi = serializers.FloatField()
    ft = serializers.FloatField()


class SearchResultSerializer(serializers.Serializer):
    text = serializers.CharField()
    pub_date = serializers.DateTimeField()
    distance = fields.SerializerMethodField('_distance')
    content_type = fields.CharField(source='model_name')
    content_object = fields.SerializerMethodField('_content_object')

    def _content_object(self, obj):
        if obj:
            if obj.model_name == 'spot':
                return SpotSerializer(obj.object, many=False, context=self.context).data
            if obj.model_name == 'collection':
                return CollectionSerializer(obj.object, many=False, context=self.context).data
            if obj.model_name == 'user':
                return SimpleUserSerializer(obj.object, many=False, context=self.context).data
        return {}

    def __init__(self, *args, **kwargs):
        self.unit = kwargs.pop('unit', None)
        return super(SearchResultSerializer, self).__init__(*args, **kwargs)

    def _distance(self, obj):
        try:
            if self.unit:
                return {self.unit: getattr(obj.distance, self.unit)}
            return DistanceSerializer(obj.distance, many=False).data
        except Exception as e:
            logging.error(e)
        return {}


class ContentLinkSerializer(serializers.ModelSerializer):
    id = fields.IntegerField(read_only=True)
    url = fields.SerializerMethodField('_getlinkurl')
    link = serializers.URLField()
    spot = serializers.SlugRelatedField(slug_field='slug')
    picture_link = serializers.URLField(required=False)
    title = serializers.CharField(required=False)
    favicon_url = fields.SerializerMethodField('_faviconurl')

    def _getlinkurl(self, obj):
        if obj:
            spot = Spot.objects.get(id=obj.spot_id)
            kwargs = {'link_id': obj.id, 'spot_slug': spot.slug}
            collection_slug = self.context['request'].parser_context['kwargs'].get('collection_slug', None)
            if collection_slug:
                kwargs.update({'collection_slug': collection_slug})

            return reverse('content-links-detail', kwargs=kwargs, request=self.context['request'],
                           format=self.context['format'])

    def _faviconurl(self, obj):
        if obj:
            return 'http://g.etfv.co/%s?defaulticon=none' % obj.link

    class Meta(object):
        model = ContentLink
        fields = ('id', 'url', 'link', 'picture_link', 'title', 'favicon_url', 'modified')
