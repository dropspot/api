#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

from django.core.management.base import BaseCommand
from dropspot.models import ContentLink
from dropspot.utilities.custom_threads import create_link_preview


class Command(BaseCommand):
    help = "Collect link preview for ContentLinks"

    def handle(self, *args, **options):
        for link in ContentLink.objects.all():
            print link.link.encode('utf-8')
            create_link_preview(link)
