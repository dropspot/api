#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

# Sys
import logging
import difflib
import time
import hashlib

# Django
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib.auth import SESSION_KEY
from django.contrib.contenttypes.models import ContentType
from django.http import Http404
from django.core.exceptions import PermissionDenied
from django.contrib.auth import login
from django.views.generic.edit import FormView
from django.core.cache import cache
from django.conf import settings
from django.contrib.gis.geos import Point as GeoPoint


# 3rd Party
from rest_framework import exceptions
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import permissions
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.request import clone_request
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import api_view, permission_classes

from allauth.socialaccount.providers.facebook.views import login_by_token
from allauth.account.models import EmailAddress
from allauth.account.utils import send_email_confirmation, setup_user_email
from allauth.account.forms import ChangePasswordForm, ResetPasswordForm

from haystack.utils.geo import Point, D
from haystack.query import SearchQuerySet, EmptySearchQuerySet
from haystack.forms import ModelSearchForm

from dropspot.serializers import (SpotSerializer, CollectionSerializer,
                                  UserSerializer, SearchResultSerializer,
                                  AvatarSerializer, ContentLinkSerializer,
                                  ProfileSerializer)

from dropspot.models import Spot, Collection, CollectionSpot, ContentLink
from subscribe.models import Subscription
from subscribe.serializers import SimpleSubscriptionSerializer

from dropspot.publishers import customPublisher

from avatar.models import Avatar

# Get an instance of a logger
logger = logging.getLogger(__name__)


API_VERSION = 'v2.1.0'


class OptionPermissionMixin(generics.GenericAPIView):
    def metadata(self, request):
        """
        Return a dictionary of metadata about the view.
        Used to return responses for OPTIONS requests.

        We override the default behavior, and add some extra information
        about the required request body for POST and PUT operations.
        """

        ret = super(OptionPermissionMixin, self).metadata(request)
        try:
            obj = self.get_object()
            for method in ret.get('actions', []):
                cloned_request = clone_request(request, method)
                self.check_object_permissions(cloned_request, obj)
                ret['actions'][method] = dict((k, v) for k, v in ret['actions'][method].items()
                                              if not v.get('read_only', True))
        except Http404:
            # Http404 should be acceptable and the serializer
            # metadata should be populated. Except this so the
            # outer "else" clause of the try-except-else block
            # will be executed.
            pass
        except (exceptions.APIException, PermissionDenied):
            del ret['actions']

        return ret


class CollectionSubscription(generics.RetrieveUpdateDestroyAPIView):
    model = Subscription
    serializer_class = SimpleSubscriptionSerializer

    exception_template_names = [
        'subscribe/subscription_%(status_code)s.html',
        '%(status_code)s.html',
        'api_exception.html'
    ]

    def get_template_names(self):
        return ['angular.html']

    def get_object(self, *args, **kwargs):
        self.collection = generics.get_object_or_404(Collection,
                                                     slug=self.kwargs['collection_slug'])
        return generics.get_object_or_404(Subscription,
                                          content_type=ContentType.objects.get_for_model(self.collection),
                                          object_id=self.collection.id,
                                          user=self.request.user)

    def put(self, request, *args, **kwargs):
        self.collection = generics.get_object_or_404(Collection,
                                                     slug=kwargs['collection_slug'])
        s, created = Subscription.objects.get_or_create(
            content_type=ContentType.objects.get_for_model(self.collection),
            object_id=self.collection.id,
            user=request.user)

        cache.delete("SubscriptionList_%s" % request.user.username)
        return super(CollectionSubscription, self).put(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        cache.delete("SubscriptionList_%s" % request.user.username)
        response = super(CollectionSubscription, self).delete(request, *args, **kwargs)
        return response


class SubscriptionList(generics.ListAPIView):
    authenticated_users_only = False
    model = Subscription
    serializer_class = SimpleSubscriptionSerializer

    def get_template_names(self):
        return ['angular.html']

    def get_queryset(self):
        username = self.kwargs.get('username', None)
        if username:
            cache_key = "SubscriptionList_%s" % username
            user_subscriptions = cache.get(cache_key)
            if not user_subscriptions:
                user = get_object_or_404(User, username=username, is_active=True)
                user_subscriptions = user.subscriptions.all()
                cache.set(cache_key, user_subscriptions)
            return user_subscriptions


class Home(APIView):
    _ignore_model_permissions = True

    def get_template_names(self):
        return ['angular.html']

    def get(self, request, *args, **kwargs):
        return Response(
            {
                'search': reverse('search',
                                  format=kwargs.get('format', None),
                                  request=request),
                'spots': reverse('spot-list',
                                 format=kwargs.get('format', None),
                                 request=request),
                'collections': reverse('collection-list',
                                       format=kwargs.get('format', None),
                                       request=request),
                'profile': reverse('profile',
                                   format=kwargs.get('format', None),
                                   request=request),
                'version': settings.VERSION,
            }
        )


class AvatarDetail(generics.RetrieveUpdateDestroyAPIView):
    authenticated_users_only = False
    serializer_class = AvatarSerializer
    model = Avatar

    def get_object(self):
        username = self.kwargs.get('username', None)
        avatar_id = self.kwargs.get('avatar_id', None)
        assert username and avatar_id

        user = get_object_or_404(User, username=username, is_active=True)
        avatar = get_object_or_404(Avatar, user=user, id=avatar_id)
        return avatar


class AvatarList(generics.ListCreateAPIView):
    authenticated_users_only = False
    serializer_class = AvatarSerializer
    model = Avatar

    def get_queryset(self):
        username = self.kwargs.get('username', None)
        if username:
            user = get_object_or_404(User, username=username, is_active=True)
            return user.avatar_set.all()
        return []

    def pre_save(self, obj):
        username = self.kwargs.get('username', None)
        if username:
            user = get_object_or_404(User, username=username, is_active=True)
            obj.user = user
        else:
            obj.user = self.request.user


class UserList(generics.ListCreateAPIView):
    permission_classes = ()
    authenticated_users_only = False
    queryset = User.objects.all()
    serializer_class = UserSerializer
    lookup_field = 'username'

    def create(self, *args, **kwargs):
        return super(UserList, self).create(*args, **kwargs)

    def get_template_names(self):
        return ['angular.html']


class UserDetail(generics.RetrieveAPIView):
    authenticated_users_only = False
    model = User
    serializer_class = UserSerializer
    lookup_field = 'username'

    def get_template_names(self):
        return ['angular.html']


class UserProfile(UserDetail, generics.RetrieveUpdateDestroyAPIView):
    authenticated_users_only = True
    serializer_class = ProfileSerializer

    def get_object(self, *args, **kwargs):
        return self.request.user


@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated])
def passwordUpdate(request):
    if not request.user.has_usable_password():
        return Response({}, status=status.HTTP_400_BAD_REQUEST)

    # change input data to match allauth form
    form_data = dict()
    form_data['oldpassword'] = request.DATA['oldpassword']
    form_data['password1'] = request.DATA['newpassword']
    form_data['password2'] = request.DATA['newpassword']

    f = ChangePasswordForm(data=form_data, user=request.user)
    if f.is_valid():
        f.save()
        return Response({'status': 'Password changed'})
    return Response(dict(f.errors.items()), status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
def passwordReset(request):
    f = ResetPasswordForm(data=request.DATA)
    if f.is_valid():
        f.save()
        return Response({'status': 'Password reset'})
    return Response(dict(f.errors.items()), status=status.HTTP_400_BAD_REQUEST)


class CollectionDetail(generics.RetrieveUpdateDestroyAPIView, OptionPermissionMixin):
    authenticated_users_only = False
    model = Collection
    serializer_class = CollectionSerializer
    slug_field = 'slug'
    slug_url_kwarg = 'collection_slug'

    def get_template_names(self):
        return ['angular.html']

    def get_object(self):
        cache_key = "CollectionDetail_%s" % self.kwargs['collection_slug']
        obj = cache.get(cache_key)
        if not obj:
            obj = super(CollectionDetail, self).get_object()
            cache.set(cache_key, obj)
        obj.current_spot = None
        spot_slug = self.kwargs.get('spot_slug', None)

        if spot_slug:
            spot = get_object_or_404(Spot, slug=spot_slug, collections=obj)
            obj.current_spot = spot

        return obj

    def pre_save(self, obj):
        if 'photo' in self.request.FILES:
            t = str(time.time()).replace(".", "-")
            filename = self.request.FILES['photo'].name.encode('utf_8')
            self.request.FILES['photo'].name = '{0}_{1}'.format(t, filename)

    def delete(self, request, *args, **kwargs):
        collection = Collection.objects.get(slug=self.kwargs['collection_slug'])
        subscriptions = Subscription.objects.filter(content_type=ContentType.objects.get_for_model(collection),
                                                    object_id=collection.id)
        for subscription in subscriptions:
            cache.delete("SubscriptionList_%s" % subscription.user.username)
            subscription.delete()

        collectionSpots = CollectionSpot.objects.filter(collection__slug=self.kwargs['collection_slug'])
        for cspot in collectionSpots:
            cspot.spot.delete()
            cspot.delete()

        return super(CollectionDetail, self).delete(request, *args, **kwargs)


class CollectionList(generics.ListCreateAPIView):
    cache_key = "CollectionList_publish"
    authenticated_users_only = False
    serializer_class = CollectionSerializer

    queryset = cache.get(cache_key)
    if not queryset:
        queryset = Collection.objects.publish()
        cache.set(cache_key, queryset)

    def get_queryset(self):
        username = self.kwargs.get('username', None)
        if username:
            user = get_object_or_404(User, username=username, is_active=True)

            cache_key = "UserCollections_%s" % user.username
            user_collections = cache.get(cache_key)
            if not user_collections:
                user_collections = user.collection_set.all()
                cache.set(cache_key, user_collections)

            return user_collections

        spot_slug = self.kwargs.get('spot_slug', None)
        if spot_slug:
            spot = get_object_or_404(Spot, slug=spot_slug)
            return spot.collections.all()

        is_profile = self.kwargs.get('is_profile', None)
        if is_profile:
            if not self.request.user.is_active:
                raise exceptions.NotAuthenticated()
            return self.request.user.collection_set.all()

        # Default: return all public Collections
        cache_key = "CollectionList_publish"
        public_collections = cache.get(cache_key)
        if not public_collections:
            public_collections = Collection.objects.publish()
            cache.set(cache_key, public_collections)
        return public_collections


    def pre_save(self, obj):
        username = self.kwargs.get('username', None)
        if username:
            user = get_object_or_404(User, username=username, is_active=True)
            obj.created_by = user
        else:
            user = self.request.user
            obj.created_by = user

    def post_save(self, obj, created=False):
        if created:
            spot_slug = self.kwargs.get('spot_slug', None)
            cache.delete("UserCollections_%s" % obj.created_by)
            cache.delete("CollectionSlugField_%s" % obj.slug)
            if spot_slug:
                spot = Spot.objects.get(slug=spot_slug)
                CollectionSpot.objects.get_or_create(collection=obj,
                                                     spot=spot,
                                                     defaults={'created_by': obj.created_by})


class SpotDetail(generics.RetrieveUpdateDestroyAPIView, OptionPermissionMixin):
    authenticated_users_only = False
    serializer_class = SpotSerializer
    model = Spot
    slug_url_kwarg = 'spot_slug'
    slug_field = 'slug'

    def get_queryset(self):
        username = self.kwargs.get('username', None)
        if username:
            user = get_object_or_404(User, username=username, is_active=True)
            cache_key = "UserSpots_%s" % user.username
            user_spots = cache.get(cache_key)
            if not user_spots:
                user_spots = user.spot_set.all()
                cache.set(cache_key, user_spots)
            return user_spots

        collection_slug = self.kwargs.get('collection_slug', None)
        if collection_slug:
            cache_key = "CollectionSpots_%s" % collection_slug
            collection_spots = cache.get(cache_key)
            if not collection_spots:
                collection = get_object_or_404(Collection, slug=collection_slug)
                collection_spots = collection.spots.all()
                cache.set(cache_key, collection_spots)
            return collection_spots

        return super(SpotDetail, self).get_queryset()

    def get_object(self):
        spot_slug = self.kwargs.get('spot_slug', None)
        if spot_slug:
            cache_key = "DetailSpot_%s" % spot_slug
            spot = cache.get(cache_key)
            if not spot:
                spot = get_object_or_404(Spot, slug=spot_slug)
                cache.set(cache_key, spot)

            return spot

    def get_template_names(self):
        return ['angular.html']

    def options(self, request, *args, **kwargs):
        """
        Handler method for HTTP 'OPTIONS' request.
        We may as well implement this as Django will otherwise provide
        a less useful default implementation.
        """
        self.object = self.get_object()
        return Response(self.metadata(request), status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        self.get_object()
        return self.update(request, *args, **kwargs)

    def pre_save(self, obj):
        if 'photo' in self.request.FILES:
            t = str(time.time()).replace(".", "-")
            filename = self.request.FILES['photo'].name.encode('utf_8')
            self.request.FILES['photo'].name = '{0}_{1}'.format(t, filename)


class SpotList(generics.ListCreateAPIView):
    authenticated_users_only = False
    model = Spot
    serializer_class = SpotSerializer

    def link_spots(self, queryset):
        fuzzy_link = self.request.QUERY_PARAMS.get('link', None)
        threshold = self.request.QUERY_PARAMS.get('link_thr', None)
        collection_slug = self.kwargs.get('collection_slug', None)
        username = self.kwargs.get('username', None)
        center = self.request.QUERY_PARAMS.get('center', None)
        publisher = self.request.QUERY_PARAMS.get('publisher', None)
        result = []

        if center:
            center = center.split(",")
        if not threshold:
            threshold = 1

        if fuzzy_link and threshold:
            clean_link = fuzzy_link.replace("http://", "").replace("https://", "").replace("www.", "").replace("m.", "")
            cache_namespace_key = "Namespace_FuzzyLinkQuery_%s" % collection_slug
            sugar = collection_slug
            if username and not collection_slug:
                cache_namespace_key = "Namespace_FuzzyLinkQuery_%s" % username
                sugar = username
            cache_namespace = cache.get(cache_namespace_key)

            if not cache_namespace:
                cache_namespace = time.time()
                # race condition
                if not cache.set(cache_namespace_key, cache_namespace):
                    cache_namespace = cache.get(cache_namespace_key)

            hash_link = hashlib.md5(clean_link)
            cache_key = "%s_%s_%s_%s-%s" % \
                        (str(cache_namespace), 'FuzzyLinkQuery', sugar, str(hash_link.hexdigest()), threshold)

            if center:
                cache_key = "%s_%s_%s_%s-%s_%s_%s" % \
                    (str(cache_namespace), 'FuzzyLinkQuery', sugar, str(hash_link.hexdigest()), threshold,
                     center[0], center[0])
            if publisher and not center:
                cache_key = "%s_%s_%s_%s_%s_%s" % \
                    (str(cache_namespace), 'FuzzyLinkQuery', sugar, str(hash_link.hexdigest()), threshold,
                     publisher)
            spot_links = cache.get(cache_key)
            if not spot_links:
                if publisher:
                    result = customPublisher(publisher, queryset, clean_link)
                else:
                    spot_links = ContentLink.objects.filter(spot__in=queryset)
                    spot_ids = []
                    for link in spot_links:
                        clean_existing_link = \
                            link.link.replace("http://", "").replace("https://", "").replace("www.", "").replace("m.", "")
                        seq = difflib.SequenceMatcher(a=clean_existing_link.lower(), b=clean_link.lower())
                        if seq.ratio() >= float(threshold):
                            spot_ids.append(link.spot.id)

                    result = Spot.objects.filter(id__in=spot_ids)
                    if center:
                        center_pnt = GeoPoint(float(center[1]), float(center[0]) )
                        result = result.distance(center_pnt).order_by('distance')

                if not result:
                    result = []
                cache.set(cache_key, list(result))
            else:
                result = spot_links

        elif fuzzy_link and not threshold:
            raise Exception("fuzzy_link threshold parameter is missing")

        else:
            result = queryset

        return result

    def get_queryset(self):
        username = self.kwargs.get('username', None)
        center = self.request.QUERY_PARAMS.get('center', None)
        if center:
            center = center.split(",")

        if username:
            user = get_object_or_404(User, username=username, is_active=True)
            cache_key = "UserSpots_%s" % user.username

            if center:
                cache_key = "UserSpots_%s_%s" % (user.username, center[0] + "-" + center[0])

            user_spots = cache.get(cache_key)
            if not user_spots:
                user_spots = user.spot_set.all()
                if center:
                    center_pnt = GeoPoint(float(center[1]), float(center[0]))
                    user_spots = user.spot_set.all().distance(center_pnt).order_by('distance')

                cache.set(cache_key, list(user_spots))

            return self.link_spots(user_spots)

        collection_slug = self.kwargs.get('collection_slug', None)
        if collection_slug:
            cache_key = "CollectionSpots_%s" % collection_slug
            if center:
                cache_key = "CollectionSpots_%s_%s" % (collection_slug, center[1] + "-" + center[0])

            collection_spots = cache.get(cache_key)
            if not collection_spots:
                collection = get_object_or_404(Collection, slug=collection_slug)
                collection_spots = collection.spots.all()
                if center:
                    center_pnt = GeoPoint(float(center[1]), float(center[0]))
                    collection_spots = collection.spots.all().distance(center_pnt).order_by('distance')
                cache.set(cache_key, list(collection_spots))

            return self.link_spots(collection_spots)

        return super(SpotList, self).get_queryset()

    def get_template_names(self):
        return ['angular.html']

    def pre_save(self, obj):
        username = self.kwargs.get('username', None)
        if username:
            user = get_object_or_404(User, username=username, is_active=True)
            obj.created_by = user
        else:
            obj.created_by = self.request.user


class AuthToken(ObtainAuthToken):
    ''' Takes a username and password and returns an API token'''

    def post(self, request):
        serializer = self.serializer_class(data=request.DATA)
        if serializer.is_valid():
            user = serializer.object['user']
            if not EmailAddress.objects.filter(user=user,
                                               verified=True).exists():

                # This is not the proper place for this code, but it
                # is the most backwards compatible place for it at the moment.
                try:
                    setup_user_email(request, user, [])
                except Exception as e:
                    logger.debug(e)
                if settings.ACCOUNT_EMAIL_VERIFICATION in ['mandatory', 'optional', True]:
                    send_email_confirmation(request, user)
                if settings.ACCOUNT_EMAIL_VERIFICATION == 'mandatory':
                    return Response(
                        {
                            'non_field_errors': [
                                'Please verify your email address by clicking on the link in the verification email.'
                            ]
                        },
                        status=status.HTTP_400_BAD_REQUEST)
            token, created = Token.objects.get_or_create(user=user)
            login(request, user)
            return Response({'token': token.key, 'username': token.user.username})

        if 'access_token' in request.POST:
            ret = login_by_token(request)
            if request.user.is_authenticated():
                token, created = Token.objects.get_or_create(user=request.user)
                return Response({'token': token.key, 'username': request.user.username})

            if ret.has_header('Location') and ret['Location'].endswith('/account/social/signup/'):  # Hack, but still.
                return Response({
                                    'access_token': 'This email address or username is already in use, please sign in at {host} and choose a different one.'.format(
                                        host=request.get_host())}, status=status.HTTP_400_BAD_REQUEST)

            return Response({'access_token': 'Please provide a valid Facebook access token'},
                            status=status.HTTP_400_BAD_REQUEST)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SearchView(generics.ListAPIView):
    authenticated_users_only = False
    serializer_class = SearchResultSerializer

    def get_template_names(self):
        return ['angular.html']

    def get_queryset(self, *args, **kwargs):
        request = self.request
        results = EmptySearchQuerySet()

        if request.GET.get('q'):
            form = ModelSearchForm(request.QUERY_PARAMS, searchqueryset=None, load_all=True)

            if form.is_valid():
                query = form.cleaned_data['q']
                results = form.search()
        else:
            form = ModelSearchForm(searchqueryset=None, load_all=True)

        # This will return a dict of the first known
        # unit of distance found in the query
        distance = None
        try:
            k, v = ((k, v) for k, v in request.QUERY_PARAMS.items()
                    if k in D.UNITS.keys()).next()
            distance = {k: v}
        except Exception as e:
            logging.error(e)

        point = None
        try:
            point = Point(float(request.QUERY_PARAMS['longitude']),
                          float(request.QUERY_PARAMS['latitude']))
        except Exception as e:
            logging.error(e)

        if distance and point:
            results = results or SearchQuerySet()
            results = results.dwithin('location', point, D(**distance)).distance('location', point).order_by('distance')

        bottom = request.QUERY_PARAMS.get('bottom', None)
        top = request.QUERY_PARAMS.get('top', None)
        right = request.QUERY_PARAMS.get('right', None)
        left = request.QUERY_PARAMS.get('left', None)
        if top and right and bottom and left:
            results = results or SearchQuerySet()
            results = results.within(
                'location',
                Point(float(left), float(bottom)),
                Point(float(right), float(top))
            )
            if point:
                results = results.distance('location', point).order_by('distance')

        model = request.QUERY_PARAMS.get('type', None)
        if model:
            if model == 'user':
                results = results.models(User)
            if model == 'spot':
                results = results.models(Spot)
            if model == 'collection':
                results = results.models(Collection)

        return results


class ContentLinksDetail(generics.RetrieveUpdateDestroyAPIView, OptionPermissionMixin):
    authenticated_users_only = False
    model = ContentLink
    serializer_class = ContentLinkSerializer

    def get_object(self, *args, **kwargs):
        self.spot = generics.get_object_or_404(Spot, slug=self.kwargs['spot_slug'])

        try:
            result = generics.get_object_or_404(ContentLink, id=self.kwargs['link_id'])
        except KeyError:
            raise Http404
        return result


class ContentLinksList(generics.ListCreateAPIView):
    authenticated_users_only = False
    model = ContentLink
    serializer_class = ContentLinkSerializer

    def get_queryset(self):
        spot_slug = self.kwargs.get('spot_slug', None)
        if spot_slug:
            spot = get_object_or_404(Spot, slug=spot_slug)
            return ContentLink.objects.filter(spot=spot).order_by('modified')
        return super(ContentLinksList, self).get_queryset()

    def pre_save(self, obj):
        spot_slug = self.kwargs.get('spot_slug', None)
        if spot_slug:
            spot = get_object_or_404(Spot, slug=spot_slug)
            obj.spot = spot


@user_passes_test(lambda u: u.is_superuser)
def su(request, username, redirect_url='/'):
    su_user = get_object_or_404(User, username=username, is_active=True)
    request.session[SESSION_KEY] = su_user.id
    return redirect(redirect_url)
