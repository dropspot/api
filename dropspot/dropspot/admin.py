#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

from django.contrib.gis import admin
from django.contrib.contenttypes import generic
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse

from . import models
from dropspot.admin_filters import CollectionPublishedFilter, SpotExpirationFilter


class UserCollectionInline(admin.TabularInline):
    model = models.Collection
    extra = 1
    fk_name = 'created_by'
    fields = ('name', 'slug', 'edit_collection')
    readonly_fields = ('name', 'slug', 'edit_collection')

    def edit_collection(self, obj):
        return mark_safe(
            '<a href="%s">%s</a>' % (reverse('admin:dropspot_collection_change',
            args=(obj.pk,)), 'Edit'))


class UserAdmin(admin.ModelAdmin):
    search_fields = ['email', 'first_name', 'last_name']
    list_display = ('email', 'first_name', 'last_name')
    list_filter = ('is_staff', 'is_superuser')

    inlines = (UserCollectionInline,)


class Collectionline(generic.GenericTabularInline):
    model = models.CollectionSpot


class SpotAdmin(admin.OSMGeoAdmin):
    list_filter = (SpotExpirationFilter,)
    search_fields = ['name']
    list_display = ['name', 'created_by', 'photo']


class CollectionSpotInline(admin.TabularInline):
    model = models.CollectionSpot
    extra = 1
    fields = ('spotname', 'edit_spot')
    readonly_fields = ('spotname', 'edit_spot')

    def spotname(self, obj):
        return obj.spot.name

    def edit_spot(self, obj):
        return mark_safe(
            '<a href="%s">%s</a>' % ( reverse('admin:dropspot_spot_change',
                args=(obj.spot.pk,)), 'Edit'))


class CollectionAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'creator']
    list_filter = (CollectionPublishedFilter,)
    inlines = (CollectionSpotInline,)
    exclude = ('modified_spots',)

    def creator(self, obj):
        return obj.created_by.username


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(models.Spot, SpotAdmin)
admin.site.register(models.Collection, CollectionAdmin)
