#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

from rest_framework.authentication import (BasicAuthentication,
                                           SessionAuthentication,
                                           TokenAuthentication)
from rest_framework import exceptions

from allauth.account.app_settings import (EmailVerificationMethod,
                                          EMAIL_VERIFICATION)
from allauth.account.models import EmailAddress
from allauth.account.utils import send_email_confirmation


def check_email_verified(request, user):
    has_verified_email = EmailAddress.objects.filter(user=user,
                                                     verified=True).exists()
    if EMAIL_VERIFICATION == EmailVerificationMethod.NONE:
        pass
    elif EMAIL_VERIFICATION == EmailVerificationMethod.OPTIONAL:
        # In case of OPTIONAL verification: send on signup.
        if not has_verified_email:
            send_email_confirmation(request, user, signup=True)
    elif EMAIL_VERIFICATION == EmailVerificationMethod.MANDATORY:
        if not has_verified_email:
            send_email_confirmation(request, user, signup=True)
            msg = 'Email address not verified'
            print msg
            raise exceptions.AuthenticationFailed(msg)


class BetterBasicAuthentication(BasicAuthentication):
    def authenticate(self, request):
        ret = super(BetterBasicAuthentication, self).authenticate(request)
        if ret:
            user = ret[0]
            check_email_verified(request, user)
        return ret


class BetterSessionAuthentication(SessionAuthentication):
    def authenticate(self, request):
        ret = super(BetterSessionAuthentication, self).authenticate(request)
        if ret:
            user = ret[0]
            check_email_verified(request, user)
        return ret


class BetterTokenAuthentication(TokenAuthentication):
    def authenticate(self, request):
        ret = super(BetterTokenAuthentication, self).authenticate(request)
        if ret:
            user = ret[0]
            check_email_verified(request, user)
        return ret
