#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

FROM ubuntu:14.04
MAINTAINER Thomas Iovine "thomas@drpspt.com"
ADD . /opt/dropspot-project
RUN apt-get -y update
RUN apt-get -y install git wget supervisor git-flow curl python-dev libffi-dev vim libpq-dev libxml2-dev libxslt-dev libzmq-dev binutils libproj-dev gdal-bin npm gettext libjpeg-dev graphviz libgraphviz-dev pkg-config
RUN wget https://raw.github.com/pypa/pip/master/contrib/get-pip.py && python get-pip.py
RUN npm config set registry http://registry.npmjs.org/
RUN npm install recess node csslint -g && ln -s /usr/bin/nodejs /usr/local/bin/node
RUN pip install virtualenvwrapper
RUN /bin/bash -c "source /usr/local/bin/virtualenvwrapper.sh && mkvirtualenv dropspot"
RUN /bin/bash -c "source /usr/local/bin/virtualenvwrapper.sh && workon dropspot && pip install -r /opt/dropspot-project/requirements.txt"
