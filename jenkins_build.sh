#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT (https://bitbucket.org/dropspot/api/src/master/LICENSE)
#

#!/usr/bin/env bash
set -e
set -x

# Get around current version permissions issues. Use TCP instead of a UNIX socket.
DOCKER="docker"

### INIT - Build the directory to be mounted into Docker.
# Create log and db to hold output and db files from the build.
# This is not that much of use for builds, but will be useful for production
# where those files are supposed to be persistent.
echo $USER

### BUILD - Build the Docker image to use for the job.
# A good idea might be to dispose of those cached images regularly, like every day.
# This stuff can be written better, but then it does not work in Jenkins because of how
# streams are being handled.

## Build the image
if [ -f ./dropspot-export.tar.gz ]; then
	rm ./dropspot-export.tar.gz
fi
$DOCKER build -rm -t jenkins/dropspot .

## Copy the local settings temaplate, it will be loaded as a docker volumn
cp ./dropspot/dropspot/local_settings/local_settings.py.template ./dropspot/dropspot/local_settings/local_settings.py

## Add jenkins specific settings
cat >> ./dropspot/dropspot/local_settings/local_settings.py <<EOF

DATABASES = {
        'default': {
            'ENGINE': 'django.contrib.gis.db.backends.postgis', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': 'dropspot',                      # Or path to database file if using sqlite3.
            # The following settings are not used with sqlite3:
            'USER': 'docker',
            'PASSWORD': 'docker',
            'HOST': '172.17.42.1',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
            'PORT': '5432',                      # Set to empty string for default.
            }
        }


# Email settings
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


RAVEN_CONFIG = {
        'dsn': 'http://ee9cea784b854222ab076322c58968ed:054f26fcc8264a40b1a115666f6e71c2@sentry.dropspot-app.com/3',
        }
RAVEN_CONFIG_DSN = RAVEN_CONFIG['dsn']


INSTALLED_APPS = INSTALLED_APPS + (
        'raven.contrib.django.raven_compat',
        )

COMPRESS_CSS_HASHING_METHOD = 'content'

EOF

### RUN
### Execute the build inside Docker.

# Run in the background so that we know the container id.
# Use the image we've just built.
CONTAINER=$($DOCKER run -d -p 8000:8000 -v `pwd`/dropspot/dropspot/local_settings/:/opt/dropspot-project/dropspot/dropspot/local_settings -t jenkins/dropspot /bin/bash -c "source /usr/local/bin/virtualenvwrapper.sh && workon dropspot && cd /opt/dropspot-project/ && dropspot/manage.py test dropspot.tests")

# Attach to the container's streams so that we can see the output.
$DOCKER attach $CONTAINER

# As soon as the process exits, get its return value.
RC=$($DOCKER wait $CONTAINER)

# $DOCKER cp $CONTAINER:/opt/dropspot-project/reports .
# Delete the container we've just used to free unused disk space.
# as well as the temporary mount directory.
$DOCKER export $CONTAINER > ./dropspot-export.tar.gz
#$DOCKER rm $CONTAINER
#sudo docker rm `sudo docker ps -a -q`
#sudo docker rmi `sudo docker images -q`

# Exit with the same value that the process exited with.
exit $RC
